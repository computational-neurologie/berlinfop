import numpy as np
import pandas as pd


def LeavePairOutFold(y, n_splits=None):
    class0 = y.index[y == 0]
    class1 = y.index[y == 1]
    test_pairs = []
    for i in class0:
        for j in class1:
            test_pairs.append((i, j))
    if n_splits is None:
        n_splits = len(test_pairs)
    test_pairs = np.array(test_pairs)
    np.random.shuffle(test_pairs)
    for k in range(n_splits):
        i, j = test_pairs[k]
        train_ind = np.append([class0[class0 != i]], [class1[class1 != j]])
        test_ind = np.array([i, j])
        np.random.shuffle(train_ind)
        np.random.shuffle(test_ind)
        yield train_ind, test_ind
