from sklearn.feature_selection import RFECV
from sklearn.model_selection import RepeatedStratifiedKFold, StratifiedKFold
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing, metrics
import numpy as np
import pandas as pd
from FO_PEG_functions import startswith_upper, get_elec_type, add_ipsilaterality
from FO_PEG_functions import correct_elec_name_error
from ml_helper_funcs import LeavePairOutFold
import ast, sys



settings = {'in_pipe_feature_reduction': True,
            # 'n_bootstraps': 500,
            # 'n_features': 12,
            't': 0.0,
            'elec_type_onset': ['PEG'],  # Electrode type where the seizure onset has to be to include the seizure
            'analyse_set': 'SOC',  # TO analyse: sSOC, (N)SOS, ALL
            'feature_file': '../data/seizure_level_outcomestandard_z_first_f.csv',
            'seizure_meta_file': '../data/Patient_seizure_list_resected_patients_control.xlsx',
            'clinical_file': '../data/Table_clinical_coded_updated.csv',
            'n_folds': 5,
            'n_repeats': 100,
            'add_clinical': ''
            }

# Change settings via arguments given to the script
for arg in sys.argv[1:]:
    key, item = arg.split('=')
    print(key, item)
    if key == 'analyse_set':
        settings[key] = item
    elif 'elec' in key:
        settings[key] = item.split('_')
    elif type(settings[key]) == type(ast.literal_eval(str(item))):
        settings[key] = ast.literal_eval(str(item))
    else:
        raise TypeError(f"Type of {key} not matched ({type(settings[key])},{type(item)})")


# Read the meta data on every seizure
seizures_data = pd.read_excel(settings['seizure_meta_file'])
# Read the clinical data for each patient
clinical_data = pd.read_csv(settings['clinical_file'], index_col=0)
clinical_data.drop(['MTS', 'Orig_localizable', 'Fifty_localizable'], axis=1, inplace=True)
print(clinical_data.columns)
# Include only patients with known outcome
clinical_data = clinical_data.loc[~pd.isnull(clinical_data['Outcome seizure free'])]
seizures_data = seizures_data.loc[np.in1d(seizures_data['Patient number'], clinical_data.index)]
# Read the channel wise feature data
full_data = pd.read_csv(settings['feature_file'], index_col=[0, 1], header=[0, 1])
# Define the scaler to normalize the data
scaler = preprocessing.StandardScaler()

# Show if there is data missing
print(f"""No data for: 
{seizures_data.loc[~np.in1d(seizures_data['File name'].values.astype(str),
                            full_data['File name'].values.astype(str)), 'File name']}""")
# Show data which has no meta data
print(f"""Meta data missing for 
{full_data.loc[~np.in1d(full_data['File name'].values.astype(str),
                        seizures_data['File name'].values.astype(str)), 'File name']}""")
# Remove data on files
full_data = full_data.loc[np.in1d(full_data['File name'].values.astype(str),
                                  seizures_data['File name'].values.astype(str))]


# Add the ipsilaterality to every electrode and remove electrodes which are not in elec_type_invest
data = pd.DataFrame()  # New frame which has only electrodes of the elec_type_invest type
# Cycle through the data file wise
for s in np.unique(full_data['File name'].values):
    tmp = full_data.loc[(full_data['File name'] == s).values].copy(deep=True)
    # Get the onset electrode and skip this data if the type is not the desired one
    onset_elec = tmp.loc[tmp['Channel onset'].values].index.get_level_values(1)
    if get_elec_type(onset_elec) not in settings['elec_type_onset']:
        print(f"{s} skipping as onset {onset_elec} not in {settings['elec_type_onset']}")
        continue

    tmp = add_ipsilaterality(tmp)
    if np.unique(tmp['Patient number']).squeeze() in [54]:
        print(np.unique(tmp['Patient number']).squeeze())
        # print(tmp['Ipsilateral'], onset_elec)
        tmp = correct_elec_name_error(tmp, onset_elec[0])
        # print(tmp['Ipsilateral'])
    tmp = tmp.loc[np.in1d(tmp['Electrode type'], settings['elec_type_onset'])]
    # Skip patients which have only ipsi or contralateral electrodes
    if all(tmp['Ipsilateral']):
        print(f'Only Ipsilateral found. Skipping {s} therefore')
        continue
    elif all(~tmp['Ipsilateral']):
        print(f'Only Contralateral found. Skipping {s} therefore')
        continue
    else:
        data = pd.concat([data, tmp])

# Extract the bands and others from the columns. Bands are lower case. Others start with upper case and help
# by splitting the data
bands = data.columns.levels[0][~startswith_upper(data.columns.levels[0])]
others = data.columns.levels[0][startswith_upper(data.columns.levels[0])]
# Exclude bands by hand
bands = bands[~np.in1d(bands, ['slow', 'fast'])]
# Condense down the features regarding their time point to ictal or preictal
full_features = pd.DataFrame()
for b in bands:
    tmp = data.loc[:, b]
    full_features[f'{b}_preictal'] = tmp.loc[:, tmp.columns.astype(float) == settings['t']-10.0].mean(axis=1)
    full_features[f'{b}_ictal'] = tmp.loc[:, tmp.columns.astype(float) == settings['t']].mean(axis=1)
# Add the clinical values to the data
full_features = pd.merge(full_features, data.loc[:, others].droplevel(axis=1, level=1),
                         left_index=True, right_index=True)
if settings['analyse_set'] == 'SOC':
    features = full_features.loc[full_features['Channel onset'].values]
elif settings['analyse_set'] == 'HOC':
    features = full_features.loc[full_features['Channel control'].values]
elif settings['analyse_set'] == 'SHOC':
    features = full_features.loc[np.logical_or(full_features['Channel onset'].values, full_features['Channel control'].values)]
elif settings['analyse_set'] == 'SOS':
    features = full_features.loc[full_features['Ipsilateral'].values]
elif settings['analyse_set'] == 'NSOS':
    features = full_features.loc[~full_features['Ipsilateral'].values]
elif settings['analyse_set'] == 'ALL':
    features = full_features
else:
    raise ValueError(f"{settings['analyse_set']} is no valid analyse setting")
    # Group on the patient seizure level
features = features.groupby('File name').mean()
# Group the feauters on the patient level to arrive at one value for each patient
features = features.groupby('Patient number').mean()
# Drop some features which were only used for determining the analyse settings, i.e., they have no clincial meaning
features.drop(['Channel onset', 'Ipsilateral', 'Channel control'], inplace=True, axis=1)

# Add the clinical features again
features = pd.merge(features, clinical_data, left_on='Patient number', right_index=True, how='left')
# Check if any feature is a nan and throw an error
if len(features.index[pd.isnull(features).any(axis=1)]) > 0:
    raise ValueError(f'Some features are nan for {features.index[pd.isnull(features).any(axis=1)]}')
    # features.drop(features.index[pd.isnull(features).any(axis=1)], inplace=True)

features.index = features.index.astype(float)
# Convenient name change
features.rename({'Outcome seizure free': 'Outcome'}, axis=1, inplace=True)
# Make the Outcome a int variable (0,1)
features['Outcome'] = (features['Outcome'] == 'Seizure free').astype('int')
# Copy the features into x which will be fed into the logistic regression
x = features.copy(deep=True)
# Reset the indices as patient ids are not needed anymore and a continuous numbering is easier
x.reset_index(inplace=True, drop=True)
# Split up the outcome as variable to test for
y = x.loc[:, ['Outcome']].squeeze()
x.drop('Outcome', inplace=True, axis=1)

x = pd.DataFrame(scaler.fit_transform(x), index=x.index, columns=x.columns)
clinical_features = x.columns[startswith_upper(x.columns)]
if settings['add_clinical'] in ['before']:
    pass
elif settings['add_clinical'] == 'only':
    x = x.loc[:, x.columns[startswith_upper(x.columns)]]  # Only clinical features
else:
    x = x.loc[:, x.columns[~startswith_upper(x.columns)]  # Only take band features
              ]

splitter = RepeatedStratifiedKFold(n_splits=settings['n_folds'], n_repeats=settings['n_repeats'], random_state=17734)
# in_pipe_splitter = RepeatedStratifiedKFold(n_splits=2, n_repeats=1, random_state=17734)

lg_model = LogisticRegression(solver='newton-cg', penalty='none')
out = pd.DataFrame(columns=np.append(['AUC', 'AUC_train', 'Accuracy', 'Accuracy_train', 'Precision',
                                      'Precision_train', 'Intercept'], x.columns))

for i, (train_index, test_index) in enumerate(splitter.split(x, y)):  # splitter.split(x, y)):
    print(f"Split {i+1}/{settings['n_folds']*settings['n_repeats']}")
    x_train, x_test = x.iloc[train_index], x.iloc[test_index]
    y_train, y_test = y.iloc[train_index].squeeze(), y.iloc[test_index].squeeze()
    x_train = pd.DataFrame(scaler.fit_transform(x_train.copy(deep=True)),
                           index=x_train.index, columns=x_train.columns)
    # Use the mean and std from the training set to transform the test set
    x_test = pd.DataFrame(scaler.transform(x_test.copy(deep=True)),
                          index=x_test.index, columns=x_test.columns)
    x_train.reset_index(inplace=True, drop=True)
    y_train.reset_index(inplace=True, drop=True)
    x_test.reset_index(inplace=True, drop=True)
    y_test.reset_index(inplace=True, drop=True)
    selector = RFECV(estimator=lg_model, scoring='roc_auc', cv=StratifiedKFold(5),
                     min_features_to_select=1, n_jobs=-1)
    selector.fit(x_train, y_train)
    selected_features = selector.get_feature_names_out()
    print(selected_features, len(selected_features))
    fpr, tpr, thr = metrics.roc_curve(y_test, selector.predict_proba(x_test)[:, 1])
    auc = metrics.auc(fpr, tpr)
    accuracy = metrics.accuracy_score(y_test, selector.predict(x_test))
    precision = metrics.precision_score(y_test, selector.predict(x_test))
    # Give metrics on training data
    fpr, tpr, thr = metrics.roc_curve(y_train, selector.predict_proba(x_train)[:, 1])
    auc_train = metrics.auc(fpr, tpr)
    accuracy_train = metrics.accuracy_score(y_train, selector.predict(x_train))
    precision_train = metrics.precision_score(y_train, selector.predict(x_train))
    tmp = pd.Series(index=out.columns, dtype=float)
    tmp.loc['Intercept'] = selector.estimator_.intercept_
    tmp.loc[selected_features] = selector.estimator_.coef_[0]
    tmp.loc['AUC'] = auc
    tmp.loc['AUC_train'] = auc_train
    tmp.loc['Accuracy'] = accuracy
    tmp.loc['Accuracy_train'] = accuracy_train
    tmp.loc['Precision'] = precision
    tmp.loc['Precision_train'] = precision_train
    out = pd.concat([out, tmp.to_frame().T])
    print(auc)
print(f"AUC: {out['AUC'].mean()} pm {out['AUC'].std()} /<#Features>: " +
      f"{(~pd.isnull(out.loc[:, x.columns])).sum().sum()/len(out)}")
out.to_csv(f"../data/rfecv_5fold_{''.join(settings['elec_type_onset'])}_{settings['analyse_set']}_{settings['t']}{settings['add_clinical']}.csv")
