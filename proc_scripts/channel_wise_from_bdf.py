#!/usr/bin/env python3.9
"""
Calculate for each channel in the given bdf files, power band measures:
measure -> median power in the band, normalized by frequency per window
zeasure -> Same but data is zscored along the t axis
"""

import os  # Operating system stuff
import pyedflib  # Library to read bdf and edf files
import numpy as np  # Library for numerics
import pandas as pd  # Library for data management (and analysis)
from datetime import datetime, date, timedelta  # Library for storing dates and times in a uniform way
import scipy.signal as scs  # Function to calculate a power spectrum and filter
from scipy.stats import zscore
import sys
from  preprocessing_funcs import notch_filter, band_pass_filter, down_sample
from FO_PEG_functions import convert_to_standard_elec_label, get_elec_type


def read_bdf(file_name, verbose=0, peg_fo_only=True):
    """Takes a file as arguement and tries to read it with bdf reader. Returns the data as pd.DataFrame and
    the sampling frequencey"""
    with pyedflib.EdfReader(file_name) as f:
        # Extract general information of the data
        header = f.getHeader()  # General Header
        annot = f.readAnnotations()  # Annotations
        signal_labels = f.getSignalLabels()  # Channel labels
        # Change signals labels to standard
        signal_labels = convert_to_standard_elec_label(signal_labels, verbose=verbose)
        # Remove all non FO/PEG

        # Signal Header #Use getSignalHeaders() to get all signal header
        if verbose > 0:
            print(f'Header: {header}')
            print(f'Channels: {signal_labels}')
            print(f'Annotation: {annot}')
        # Read all sampling frequencies and check if they are the same
        fs_list = []
        for i, ch in enumerate(signal_labels):
            fs_list.append(f.getSignalHeader(i)['sample_frequency'])
        if len(set(fs_list)) != 1:
            raise ValueError('Different sampling frequencies found in the data: {fs_list}')
        fs = fs_list[0]
        # Extract channels/electrode specific information
        # Read in the electrophyiscal data
        # Define DataFrame into which the data is saved
        signals_data = pd.DataFrame()
        # Loop through the interesting channels and save them to the DataFrame
        for i, ch in enumerate(signal_labels):
            if peg_fo_only:
                if ~np.in1d(get_elec_type(ch), ['FO', 'PEG']):
                    continue
            signals_data.loc[:, ch] = f.readSignal(i, digital=False)
        signals_data = signals_data.astype(float)
        signals_data.index /= fs
        signals_data.index.name = 'Time'
        return signals_data, fs

verbose = 0
save_path = '/sc-external/sc-computational-neurology/foramen_ovale_project/data'
save_path = '../data'
psd_window_width = 10  # (in sec) length of the window on which we will calculate power spectral densities
psd_window_overlap = 0.0  # (in fractions of the window length) length of the window overlap for psd intensities
down_sample_to = 256  # Frequency to which down sampling should occur
nperseg = down_sample_to//2
# Define at which part of the data you want to look at. Start from time start_t up to time end_t.

# start_t, end_t = 300, 900  # This is in absolute seconds of the recording
start_t, end_t = - 90, + 10  # This will be added to the seizure onset
# Make the time points to evaluate relative to the seizure from this
time_points = np.arange(start_t, end_t, psd_window_width * (1 - psd_window_overlap))
# Frequencies of the different bands of interest
# freqs = ((0.1, 0.5), (0.5, 2), (2, 4), (4, 8), (8, 12), (13, 30), (30, 45), (55, 95),  (0.5, 13), (20, 45))
freqs = ((2, 4), (4, 8), (8, 12), (13, 30), (30, 45), (55, 95))
# Names of these bands
# bands = ('low_delta', 'medium_delta', 'delta', 'theta', 'alpha', 'beta', 'gamma', 'high_gamma', 'slow', 'fast')
# ratios = ('high_gamma/low_delta', 'delta/low_delta')
bands = ('delta', 'theta', 'alpha', 'beta', 'gamma', 'high_gamma')
ratios = ()
# Columns of the data (Must be double index for one band and one time point
data_columns = pd.MultiIndex.from_product((np.append(bands, ratios), time_points), names=('band', 'time'))
# Read the seizures file and its subsheets
xls_path = f'{save_path}/Patient_seizure_list_resected_patients_control.xlsx'
xls = pd.ExcelFile(xls_path)
df1 = pd.read_excel(xls, 'Recommended resection')
# df2 = pd.read_excel(xls, 'Not recommended resection')
xls1 = pd.ExcelFile(f'{save_path}/Clinical_outcome.xlsx')
clinical = pd.read_excel(xls1, 'Sheet1')
if len(np.unique(clinical['Patient number'].values)) != len(clinical['Patient number']):
    raise ValueError('There are multiple rows for the same patient')

# Read the data files
for file_name in sys.argv[1:]:  # 72269.eeg.bdf  ['../data/AN0031.EEG.bdf']: #['../data/563640.eeg.bdf']: #
    if os.path.isfile(file_name):
        pass
    else:    
        print(f'{file_name} does not exist and will be skipped')
        continue
        # raise FileNotFoundError(f'{file_name} does not exist')
    file = file_name.split('/')[-1].split('.')[0]
    if not str(file) in df1['File name'].values.astype(str):
        print(f'File {file} not in {xls_path} and will be skipped')
        continue
    print(f'Processing {file_name}')
    
    #  Get the meta info from the excel file
    file_info = df1.loc[df1['File name'].astype(str) == file]
    if verbose > 0:
        print(file)
        print(f"Processing {file} of patient {file_info['Patient number'].squeeze()}")
        print(clinical.loc[clinical['Patient number'].squeeze() == file_info['Patient number'].squeeze()])
        # If the data file does not exists skip it
        print(file_info)
    # Add a seizure column to the data which is True whenever there was a seizure at this time stamp, False if not
    onset = (datetime.combine(date.min, file_info['Seizure onset time'].squeeze()) -
             datetime.combine(date.min, file_info['File start recording'].squeeze())).total_seconds()
    offset = (datetime.combine(date.min, file_info['Seizure offset time'].squeeze()) -
              datetime.combine(date.min, file_info['File start recording'].squeeze())).total_seconds()
    if verbose > 0:
        print(f'Seizure onset {onset} and offset {offset}')
    # Read the data
    data, fs = read_bdf(file_name, verbose=verbose)

    # Check for unique
    unique_threshold = 10 # down_sample_to*1
    remove_chs = np.full(fill_value=False, shape=len(data.columns))
    for t in time_points:  # Check if there are at leas unique_threshold unique values in every time frame
        unique_values = data.loc[t + onset:t + onset + psd_window_width].apply(lambda x: len(np.unique(x)))
        remove_chs = np.logical_or(remove_chs, unique_values < unique_threshold)
    # Convert the boolean array to channel names
    remove_chs = data.columns[remove_chs]
    # Actually remove the channels
    if len(remove_chs) > 0:
        print(f'{remove_chs} have to be removed as they not have enough unique data points')
        if verbose > 0:
            print(f'{unique_values.loc[remove_chs]}')
    data.drop(remove_chs, inplace=True, axis=1)
    if len(data.columns) == 0:
        print(f'Skipping file {file_name} entirely due to data being rather constant')
        continue
    # Filter out power line noise
    data = notch_filter(data, fs)
    # Filter out low and high frequencies
    data = band_pass_filter(data, 0.1, 100, fs)
    # Down sample the the signal to the target frequency
    data = down_sample(data, data_fs=int(fs), target_fs=down_sample_to)

    # Make the data relative to the seizure onset
    data.index -= onset

    # Calculate the power spectral density on short segments of length psd_window_length
    # Number of segments per welch window. This defines the frequency resolution to sample_frequency/nperseg


    # Cut the data to what we are interested in
    data = data.loc[start_t:end_t+psd_window_width]
    # Define the output frames
    data_out = pd.DataFrame(index=data.columns, dtype=float, columns=data_columns)
    data_out.index.name = 'Channel'
    zdata_out = data_out.copy(deep=True)

    # Cycle through the corresponding time steps
    for ch in data.columns:
        data_constant = False  # This flag makes sure that the data is not constant. If it is constant in any time
        # window the channel will be exclueded
        data_windowed_psd = pd.DataFrame(columns=np.arange(0, down_sample_to/2, down_sample_to / nperseg))
        data_windowed_psd.name = ch
        for i in time_points:
            # Skip ch if data is constant
            if len(np.unique(data.loc[i:(i+psd_window_width), ch])) == 1:
                data_constant = True
                break
            if len(data.loc[i:(i+psd_window_width), ch]) < nperseg:
                print(f'Skipping {i} as there is not enough data')
                continue
            # Calculate the power spectral density on this time steps
            w, pw = scs.welch(zscore(
                              data.loc[i:(i+psd_window_width), ch].values,),
                              fs=down_sample_to, window='hanning', nfft=None, detrend='constant', noverlap=nperseg//2,
                              scaling='density', axis=-1, return_onesided=True, nperseg=nperseg)
            tmp = pd.Series(pw, index=w, name=i)
            # Normalize by frequency
            # Parse the psd into the frame
            data_windowed_psd = pd.concat([data_windowed_psd, tmp.to_frame().T], ignore_index=False, axis=0)
        if data_constant:
            print(f'Channel {ch} has to be exclueded as the data is constant -> PSD is undefined')
            continue
        # Calculate Z scores for each time window
        data_windowed_zpsd = data_windowed_psd.apply(lambda x: x/np.sum(x), axis=1)
        # Create a dataframe that contains the average of Z scores for ratios and frequency bands

        # Calculate all median band powers and add them to our output
        for band, f in zip(bands, freqs):
            tmp = data_windowed_psd.iloc[:, np.logical_and(data_windowed_psd.columns >= f[0],
                                         data_windowed_psd.columns < f[1])].median(axis=1)
            data_out.loc[ch, (band, slice(None))] = tmp.values
            tmp = data_windowed_zpsd.iloc[:, np.logical_and(data_windowed_zpsd.columns >= f[0],
                                          data_windowed_zpsd.columns < f[1])].median(axis=1)
            zdata_out.loc[ch, (band, slice(None))] = tmp.values
        # Add the ratios to the outframe
        for r in ratios:
            num, den = r.split('/')
            tmp = data_out.loc[ch, (num, slice(None))].values / data_out.loc[ch, (den, slice(None))].values
            data_out.loc[ch, (r, slice(None))] = tmp
            tmp = zdata_out.loc[ch, (num, slice(None))].values / zdata_out.loc[ch, (den, slice(None))].values
            zdata_out.loc[ch, (r, slice(None))] = tmp

    # Save the data for each patient
    data_out.to_csv(f"{save_path.replace('raw_data', 'data')}/{file}_standard_zeasure.csv")
    zdata_out.to_csv(f"{save_path.replace('raw_data', 'data')}/{file}_standard_zfeasure.csv")
