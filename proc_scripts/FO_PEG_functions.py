#!/usr/bin/env python3.9
"""
Put all functions which will be used more often here
"""
import pandas as pd
import scipy.signal as scs
import numpy as np
import scipy.stats as stat


# Channel name manipulation and type evaluation
def get_elec_type(ch):
    """
    Give the type of the electrode, FO, PEG, Other, EKG
    """

    if ch.startswith('FO'):
        if ch.endswith('ipsi') or ch.endswith('contra'):
            return 'Lat_FO_med'
        else:
            return 'FO'
    elif ch in ['PEG_ipsi', 'PEG_contra']:
        return 'Lat_PEG_med'
    elif ch.startswith('P0'):
        return 'PEG'
    elif ch.startswith('P1'):
        if len(ch) <= 2:
            return 'Other'
        elif ch[2].isdigit():
            return 'PEG'
        else:
            return 'Other'
    elif ch.startswith('EKG'):
        return 'EKG'
    else:
        return 'Other'


get_elec_type = np.vectorize(get_elec_type)  # Allow for iterable inputs


def convert_to_standard_elec_label(ch_list, verbose=0):
    """
    Convert to standard elec names, except when both are in the list
    ['F01']->['FO1'] but   ['F01', 'FO1']-  ['F01', 'FO1']
    ['PO1']->['P01']
    """
    changed_channels = []
    new_ch_list = []
    for i, ch in enumerate(ch_list):
        # Change FO channels
        if ch.replace('F0', 'FO') != ch:  # Do nothing if the channel was not altered
            if ch.replace('F0', 'FO') in ch_list:  # Check if the altered version already existed in the channels, if so revert to ch
                new_ch_list.append(ch)
            else:
                new_ch_list.append(ch.replace('F0', 'FO'))
                changed_channels.append((ch, ch.replace('F0', 'FO')))
        # Change PEG channels
        elif ch.replace('PO', 'P0') != ch:  # Do nothing if the channel was not altered
            if ch.replace('PO', 'P0') in ch_list:   # Check if the altered version already existed in the channels, if so revert to ch
                new_ch_list.append(ch)
            else:
                new_ch_list.append(ch.replace('PO', 'P0'))
                changed_channels.append((ch, ch.replace('PO', 'P0')))
        else:
            new_ch_list.append(ch)
    if verbose > 0:
        if len(changed_channels) > 0:
            print(f'Channels changed (from, to): {changed_channels}')
    return new_ch_list


def get_elec_side(ch):
    """Even is right = True"""
    dig = ''.join([s for s in ch if s.isdigit()])
    if dig == '':
        return ''
    elif int(dig) % 2 == 0:
        return 'Right'
    else:
        return 'Left'


get_elec_side = np.vectorize(get_elec_side)


def startswith_upper(txt):
    if txt[0].isupper():
        return True
    else:
        return False


startswith_upper = np.vectorize(startswith_upper)


def unordered_unique(iterable):
    _, inds = np.unique(iterable, return_index=True)
    return np.array(iterable[sorted(inds)])


def add_ipsilaterality(seizure_df):
    """Add a Ipsilateral column to the dataframe. Number of seizures and seizure onset electrodes must be one or an
    Error is thrown.
    """
    if len(np.unique(seizure_df['File name'])) != 1:
        raise ValueError(f"Exactly one seizrue should be given. {len(np.unique(seizure_df['File name']))} where found.")
    onset_elec = seizure_df.loc[seizure_df['Channel onset'].values].index.get_level_values(1)
    if len(onset_elec) == 1:
        onset_elec = onset_elec[0]
    else:
        raise ValueError(f"""Number of onset electrodes must be exactly 1 but {len(onset_elec)} where found. 
For {np.unique(seizure_df['File name'])}""")
    onset_side = get_elec_side(onset_elec)
    seizure_df['Ipsilateral'] = get_elec_side(seizure_df.index.get_level_values(1)) == onset_side
    return seizure_df


def correct_elec_name_error(seizure_df, onset_elec):
    """Change Ipsilateral as there is an error in the sides of the FOs (1-3 == Left and 4-6 == Right)"""
    if seizure_df.loc[(slice(None), [onset_elec]), 'Electrode type'].squeeze() == 'PEG':
        if get_elec_side(onset_elec) == 'Right':
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = False
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = True
        elif get_elec_side(onset_elec) == 'Left':
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = True
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = False
        else:
            raise ValueError(f"Cannot determine side of onset {onset_elec}")
    elif seizure_df.loc[(slice(None), [onset_elec]), 'Electrode type'].squeeze() == 'FO':
        if get_elec_side(onset_elec) == 'Left' and onset_elec[-1] in ['1', '2', '3']:
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = True
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = False
        elif get_elec_side(onset_elec) == 'Right' and onset_elec[-1] in ['4', '5', '6']:
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = False
            seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = True
        else:
            seizure_df.loc[:, 'Ipsilateral'] = ~ seizure_df.loc[:, 'Ipsilateral']
            if onset_elec[-1] in ['1', '2', '3']:
                seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = True
                seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = False
            elif onset_elec[-1] in ['4', '5', '6']:
                seizure_df.loc[(slice(None), [f'FO{i}' for i in range(1, 4)]), 'Ipsilateral'] = False
                seizure_df.loc[(slice(None), [f'FO{i}' for i in range(4, 7)]), 'Ipsilateral'] = True
            else:
                raise Exception(f' Should not occur. Error catching went wrong. Check manual for {seizure_df}')

    else:
        raise ValueError(f"Seizure onset electrode type not in FO, PEG " +
                         f"({seizure_df.loc[(slice(None), [onset_elec]), 'Electrode type'].squeeze()})")
    return seizure_df


def main():
    pass


if __name__ == '__main__':
    main()
