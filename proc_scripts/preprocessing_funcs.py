#!/usr/bin/env python3.9
"""
Put all functions which will be used more often here
"""
import pandas as pd
import scipy.signal as scs
import scipy.stats as stats
import numpy as np
import scipy.stats as stat


# Utility
def normalize(x):
    """ Normalize data to [0,1]"""
    return (x-np.min(x))/(np.max(x)-np.min(x))



# Raw data preprocessing (Filters, Downsampling)
def notch_filter(data, sample_freq, cutoff_hz=(47.5, 52.5, 97.5, 102.5)):
    """
    Notch filter frequencies f0-f1, f2-f3, f4-f5,...etc .
    Default frequencies between 47.5-52.5, 97.5-102.5 Hz are cut out
    """
    nyq_rate = (sample_freq/2)  # calculate signal Nyquist frequency
    width = .6/nyq_rate  # filter width
    dip = 15.0  # desired attenuation
    # compute filter coefficients
    order, beta = scs.kaiserord(dip, width)
    order = order | 1
    # build bandpass window array normalized by nyquist frequency
    cutoff_hz_nyq = np.array(cutoff_hz)/nyq_rate
    # Create the finite impulse response filter from the window settings and cut off frequencies
    taps = scs.firwin(order, cutoff_hz_nyq, window=('kaiser', beta), pass_zero='bandstop')
    # Apply the filter forward and backard on all channels excluding the Time axis
    if isinstance(data, pd.DataFrame):
        out = data.apply(lambda d: scs.filtfilt(taps, [1.0], d))
    else:
        out = pd.Series(scs.filtfilt(taps, [1.0], data))
    return out


def band_pass_filter(data, lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = scs.butter(order, [low, high], analog=False, btype='band', output='sos')
    if isinstance(data, pd.DataFrame):
        out = data.apply(lambda d: scs.sosfiltfilt(sos, d))
    else:
        out = pd.Series(scs.sosfiltfilt(sos, data))
    return out


def down_sample(data, data_fs, target_fs=256):
    """Downsample data from its frequency data_fs to target_fs. Potential upsampling will be done by
    scipy.signal.resample_poly"""
    gcd = np.gcd(target_fs, data_fs)
    up_sample_factor = target_fs/gcd
    down_sample_factor = data_fs/gcd
    if isinstance(data, pd.DataFrame):
        out = data.apply(lambda d: scs.resample_poly(d, up_sample_factor, down_sample_factor))
    else:
        out = pd.Series(scs.resample_poly(data, up_sample_factor, down_sample_factor))
    out.reset_index(inplace=True, drop=True)
    out.index /= target_fs
    return out


def check_for_uniques(data, thres=2.):
    """ Check if more than thres values are repeated in the data"""
    return len(set(data)) >= (len(data) - thres)


def mask_constant_to_nan(data, constant_threshold=20, coarsity=1):
    """Check if data has more than constant_threshold times the same value in a row. Only examine every coursaity step"""
    if isinstance(data, pd.Series):
        data = data.to_frame()
    out = np.full(shape=data.shape, fill_value=False)
    for c in range(len(data.columns)):
        t = 0
        while t < len(data):
            i = coarsity
            if out[t, c]:
                continue
            else: 
                while t+i < len(data):
                    if data.iloc[t+i, c] != data.iloc[t, c]:
                        break
                    else:
                        i += coarsity
                        
            if i >= constant_threshold:
                out[t:t+i+1,c] = np.nan
                i += 1
            t = t+i
    return out.squeeze()


# Vigilance index calculation
def get_vi_reed(data, fs, welch_res=1, welch_overlap=0.5, welch_detrend='constant', welch_window='hanning',
                band_power_window_length_in_secs=30, not_uniques_thres_frac=16, **kwargs):
    """
    Calculate the vigilance index as in Reed et al., 2017 as ratio of frequency powers
    (theta+delta+spindle)/(alpha+high_beta)
    band_power_window_length_in_secs:  window on which the powers are calculated (defaults to 30s as in Reed)
    other arguments as in psd_welch ->
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """

    result = pd.Series(name='VI_Reed')
    result.index.name = 'Time'
    # Convert the window length from sec to ticks
    window_ticks = int(band_power_window_length_in_secs*fs)
    # Calculate time points where the power is assessed, every band_power_window_length_in_secs
    time_in_ticks = np.arange(start=0, stop=len(data['Time']), step=int(window_ticks))
    for T in time_in_ticks:
        segment = data.iloc[T:T+window_ticks].copy()
        if len(segment) != window_ticks:
            continue
        segment.reset_index(inplace=True, drop=True)
        # Calculate the psd
        psd = psd_welch(segment, fs=fs, welch_res=welch_res, welch_detrend=welch_detrend, welch_window=welch_window,
                        welch_overlap=welch_overlap, not_uniques_thres_frac=not_uniques_thres_frac, norm=True)
        psd = psd.mean(axis=1)
        # Explicitly calculate the VI as in Reed et al., 2017
        # delta power
        delta_array = np.logical_and(psd.index >= 1, psd.index <= 4)
        delta_power = psd.loc[delta_array].sum() 
        # theta power
        theta_array = np.logical_and(psd.index >= 4, psd.index <= 7)
        theta_power = psd.loc[theta_array].sum() 
        # spindle power
        spindle_array = np.logical_and(psd.index >= 11, psd.index <= 16)
        spindle_power = psd.loc[spindle_array].sum() 
        # alpha power
        alpha_array = np.logical_and(psd.index >= 8, psd.index <= 13)
        alpha_power = psd.loc[alpha_array].sum() 
        # high beta power
        high_beta_array = np.logical_and(psd.index >= 20, psd.index <= 40)
        high_beta_power = psd.loc[high_beta_array].sum()
        # Set output to np.nan if any power was np.nan (probably due to not enough unique values in data)
        if (np.array([alpha_power, high_beta_power, spindle_power, theta_power, delta_power]) == 0).any():
            result.at[data.iloc[T].loc['Time']] = np.nan
        else:
            result.at[data.iloc[T].loc['Time']] = (delta_power+theta_power+spindle_power)/(alpha_power+high_beta_power)
    return result

# PSD
def psd_welch(data, fs, norm=False, welch_res=1, welch_overlap=0.5,
              welch_detrend='constant', welch_window='hanning', not_uniques_thres_frac=16, **kwargs):
    """
    Calculate the power spectrum along each column/channel (ignoring Time) using welchs method,
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    norm: (bool) if the psd should be normalized
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """
    # Define the length of the windows used to calculate the power spectrum with welchs method
    nperseg = len(data)
    if welch_res is not None:
        nperseg = fs // welch_res
    # Calculate the overlap of the windows
    local_overlap = nperseg * welch_overlap
    # Check if column has enough unique values to calculate a psd
    calc_psd_at = data.columns[[check_for_uniques(data[ch],
                                thres=len(data) / not_uniques_thres_frac) for ch in data.columns]]
    # Exlude the time for psd calculation
    calc_psd_at = calc_psd_at[calc_psd_at != 'Time']
    # Calculate the frequency bins of the psd (necessary because calc_psd_at can be empty
    freqs = np.arange(0, int(1+nperseg/2))*fs/nperseg
    # Define the DataFrame to save the psd into
    psd = pd.DataFrame(columns=data.columns[data.columns != 'Time'], index=freqs)
    psd.index.name = 'Frequency'
    # Calculate the psd using welchs method
    if len(calc_psd_at) > 0:
        w, pw = scs.welch(data[calc_psd_at].T, fs=fs, window=welch_window, nperseg=nperseg,
                          noverlap=local_overlap, nfft=None, detrend=welch_detrend,
                          scaling='spectrum', axis=-1, return_onesided=True)
        psd[calc_psd_at] = pw.T
    # Normalize
    if norm:
        psd[calc_psd_at] = psd[calc_psd_at].apply(lambda x: np.full(len(x),np.nan) if np.all(x==0)  else x/np.sum(x))
    return psd


# Average band power measure
def get_band_power(data, fs, bands, pre_psd_func=lambda x: x, post_psd_func=np.median, 
                   norm_psd=True, welch_res=1, welch_overlap=0.5, welch_detrend='constant', welch_window='hanning', 
                   not_uniques_thres_frac=16):
    """
    Calculate the power in certain frequency bands and use some averaging method (mean, median)
    bands: ((f0,f1), (f2,f3), ...) band borders to calculate the power in
    pre_psd_per_seg: How to handle each segement before psd calculation (must retain the format of the data)
                     default: f(x)=x
    post_psd_per_seg: How to handle each segement per band after psd calculation (must reduce dimention by one)
                      default : np.median
    
    other args as in psd_welch ->
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    norm: (bool) if the psd should be normalized
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    """
    result = pd.DataFrame(columns=data.columns)
    # Calculate psd on the whole given data
    psd = psd_welch(data.apply(pre_psd_func), fs=fs, welch_res=welch_res, welch_detrend=welch_detrend, 
                    welch_window=welch_window, welch_overlap=welch_overlap, not_uniques_thres_frac=not_uniques_thres_frac, 
                    norm=norm_psd)
    # Go through all desired bands and calculate the mean/median (log) power
    for band in bands:
        band_array = np.logical_and(psd.index >= band[0], psd.index <= band[1])
        band_psd = psd.loc[band_array, psd.columns != 'Frequency']
        # apply the function post psd calculation on the segment
        band_power = band_psd.apply(lambda x: np.nan if pd.isnull(x).any() else post_psd_func(x))
        band_power.name = band
        result = pd.concat([result, band_power.to_frame().T])
        
    return result


# Apply function segment wise
def get_segment_wise(data, fs, segment_length_in_sec, seg_wise_func):
    """
    Segment the data and calculate the seg_wise_func on each segment
    data: Input time series (should be dataframe with index being the time)
    fs: sample frequency
    segment_length_in_sec: Length of the desired segemnts. Only segments of full length are processed
    seg_wise_func: Function which takes data and sample frequency as input 
                  
    """
    result = pd.DataFrame()
    # Convert the window length from sec to ticks
    window_ticks = int(segment_length_in_sec*fs)
    # Calculate time points where the power is assessed
    time_in_ticks = np.arange(start=0, stop=len(data), step=int(window_ticks))
    
    # Calculate the mean power in bands
    for T in time_in_ticks:
        segment = data.iloc[T:T+window_ticks]  # segment the data
        if len(segment) != window_ticks:  # This will exclude the last segment if it is too short
            continue
      #  segment.reset_index(inplace=True, drop=True)
        # Calculate the band power
        band_power = seg_wise_func(segment, fs)
        # Add time point to full frame
        band_power.index = pd.MultiIndex.from_product([[T/fs+data.index[0]], band_power.index])
        result = pd.concat([result, band_power])
    return result


# Correlation measures
def _estimated_autocorrelation_1d(x):
    """ Estimate the autocorrelation of x
     https://stackoverflow.com/questions/14297012/estimate-autocorrelation-using-python"""
    if pd.isnull(x).all():  # Return np.nan if data is only np.nan
        return np.full(len(x), np.nan)
    n = len(x)
    variance = x.var()
    x = x-x.mean()  # Center data around mean
    r = np.correlate(x, x, mode='full')[-n:]  # Self correlate the data (gets less good the larger lag )
    result = r/(variance*(np.arange(n, 0, -1)))  # Normalize through variance and  len of overlap
    return result


def estimated_autocorrelation(data):
    """Calculate the autocorrelation column wise and rename the ndex to lag"""
    auto_result = data.copy(deep=True).apply(_estimated_autocorrelation_1d)
    auto_result.index.name = 'Lag'
    auto_result.index = data.index - data.index[0]
    return auto_result  


def lag_x(data, x=1):
    """Give a seris with only the values at the x'th lag"""
    raw = data.drop(['Time', 'Lag'], errors='ignore', axis=1)
    result = raw.iloc[[x]]  # Extract the x'th lag value
    result.set_index(np.array([f'Lag{x}']), inplace=True, drop=True)
    result = result.squeeze(axis=0)  # Convert DataFrame to Series
    return result


def hwhm(data, hmfunc=lambda data: hm_from_lag_x_med_3_2(data, x=1)):
    """
    Calculate the value when the data falls first time below HM and HM.
    hmfunc should take exactly one argument the data
    """
    # If Time is available we can return the lag in units of time. If it is not available the lag will be an integer
    if 'Time' in data.columns:
        dt = (np.sum(data['Time'][1:])-np.sum(data['Time'][:-1]))/(len(data['Time'])-1)
    elif 'Lag' in data.columns:
        dt = (np.sum(data['Lag'][1:])-np.sum(data['Lag'][:-1]))/(len(data['Lag'])-1)
    elif data.index.name in ['Time', 'Lag']:
        dt = (np.sum(data.index[1:])-np.sum(data.index[:-1]))/(len(data.index)-1)
    else:
        dt = 1
    # For some reason if it is not done like this inplace changes were observed
    data = data.copy().drop(['Time', 'Lag'], errors='ignore', axis=1)
    # Calculate the hm
    hm = hmfunc(data)
    # Calculate the first value when the data drops below the hm
    ind = data.lt(hm).apply(lambda x: np.nan if ~ (x.any()) else np.argmax(x))
    # Scale by dt
    ind = ind*dt
    ind = ind.rename(f'HW{hm.name}')
    return ind, hm


def hm_from_lag_x_med_3_2(data, x=1):
    """
    Take average between 1/3 and 1/2 of the data as minimum for HM calculation
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x +
              data.iloc[len(data)//3:len(data)//2].apply(lambda y: np.nan if np.isnan(y).any() else np.median(y)))/2
    result.name = f'HMmed1/3to1/2from{at_lag_x.name}'
    return result
